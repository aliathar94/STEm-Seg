import torch
import torch.nn as nn

import math


class PositionEmbeddingSine(nn.Module):
    """
    This is a more standard version of the position embedding, very similar to the one
    used by the Attention is all you need paper, generalized to work on images.
    """
    def __init__(self, num_pos_feats=64, temperature=10000, normalize=False, scale=None):
        super().__init__()
        self.num_pos_feats = num_pos_feats
        self.temperature = temperature
        self.normalize = normalize
        if scale is not None and normalize is False:
            raise ValueError("normalize should be True if scale is passed")
        if scale is None:
            scale = 2 * math.pi
        self.scale = scale

    def forward(self, x, mask=None):
        if mask is None:
            b, _, h, w = x.shape
            mask = torch.zeros(b, h, w, dtype=torch.bool, device=x.device)

        not_mask = ~mask
        y_embed = not_mask.cumsum(1, dtype=x.dtype)
        x_embed = not_mask.cumsum(2, dtype=x.dtype)
        if self.normalize:
            eps = 1e-6
            y_embed = y_embed / (y_embed[:, -1:, :] + eps) * self.scale
            x_embed = x_embed / (x_embed[:, :, -1:] + eps) * self.scale

        dim_t = torch.arange(self.num_pos_feats, dtype=x.dtype, device=x.device)
        dim_t = self.temperature ** (2 * (dim_t // 2) / self.num_pos_feats)

        pos_x = x_embed[:, :, :, None] / dim_t
        pos_y = y_embed[:, :, :, None] / dim_t
        pos_x = torch.stack((pos_x[:, :, :, 0::2].sin(), pos_x[:, :, :, 1::2].cos()), dim=4).flatten(3)
        pos_y = torch.stack((pos_y[:, :, :, 0::2].sin(), pos_y[:, :, :, 1::2].cos()), dim=4).flatten(3)
        pos = torch.cat((pos_y, pos_x), dim=3).permute(0, 3, 1, 2)

        return pos


class SpatioTemporalEmbeddingSine(nn.Module):
    """
        This is a more standard version of the position embedding, very similar to the one
        used by the Attention is all you need paper, generalized to work on images.
        """

    NUM_DIM_FEATS = {
        64:  (16, 24, 24),
        128: (32, 48, 48),
        256: (64, 96, 96)
    }

    def __init__(self, num_pos_feats=128, temperature=10000, normalize=False, scale=None):
        super().__init__()
        assert num_pos_feats in self.NUM_DIM_FEATS

        self.num_pos_feats = self.NUM_DIM_FEATS[num_pos_feats]
        self.temperature = temperature
        self.normalize = normalize
        if scale is not None and normalize is False:
            raise ValueError("normalize should be True if scale is passed")
        if scale is None:
            scale = 2 * math.pi
        self.scale = scale

    def forward(self, x, mask=None):
        """
        Forward method
        :param x: tensor of shape [B, C, T, H, W]
        :return: position embedding tensor of shape [B, C, T, H, W]
        """
        if mask is None:
            b, _, t, h, w = x.shape
            mask = torch.zeros(b, t, h, w, dtype=torch.bool, device=x.device)

        not_mask = ~mask
        t_embed = not_mask.cumsum(1, dtype=torch.float32)
        y_embed = not_mask.cumsum(2, dtype=torch.float32)
        x_embed = not_mask.cumsum(3, dtype=torch.float32)

        if self.normalize:
            eps = 1e-6
            t_embed = t_embed / (t_embed[:, -1:, :, :] + eps) * self.scale
            y_embed = y_embed / (y_embed[:, :, -1:, :] + eps) * self.scale
            x_embed = x_embed / (x_embed[:, :, :, -1:] + eps) * self.scale

        dim_temperatures = []
        for i in range(3):
            dim_t = torch.arange(self.num_pos_feats[i], dtype=torch.float32, device=x.device)
            dim_temperatures.append(self.temperature ** (2 * (dim_t // 2) / self.num_pos_feats[i]))

        pos_t = t_embed[:, :, :, :, None] / dim_temperatures[0]
        pos_x = x_embed[:, :, :, :, None] / dim_temperatures[1]
        pos_y = y_embed[:, :, :, :, None] / dim_temperatures[2]

        pos_t = torch.stack((pos_t[:, :, :, :, 0::2].sin(), pos_t[:, :, :, :, 1::2].cos()), dim=5).flatten(4)
        pos_x = torch.stack((pos_x[:, :, :, :, 0::2].sin(), pos_x[:, :, :, :, 1::2].cos()), dim=5).flatten(4)
        pos_y = torch.stack((pos_y[:, :, :, :, 0::2].sin(), pos_y[:, :, :, :, 1::2].cos()), dim=5).flatten(4)

        pos = torch.cat((pos_t, pos_y, pos_x), dim=4).permute(0, 4, 1, 2, 3)
        return pos
