from torch import Tensor
from typing import Optional
from stemseg.modeling.embedding_utils import add_spatiotemporal_offset, get_nb_embedding_dims, get_nb_free_dims
from stemseg.modeling.common import UpsampleTrilinear3D, get_temporal_scales, get_pooling_layer_creator
from stemseg.modeling.exp_decoders.positional_encoding import SpatioTemporalEmbeddingSine
from stemseg.utils.global_registry import GlobalRegistry

import torch
import torch.nn as nn
import torch.nn.functional as F

EMBEDDING_HEAD_REGISTRY = GlobalRegistry.get("EmbeddingHead")


class VanillaAttentionLayer(nn.Module):
    def __init__(self, in_channels):
        super().__init__()

        self.norm1 = nn.LayerNorm(in_channels)
        self.fc1 = nn.Linear(in_channels, in_channels)
        self.dropout1 = nn.Dropout(0.1)

        self.mha = nn.MultiheadAttention(in_channels, 8)

        self.norm2 = nn.LayerNorm(in_channels)
        self.fc2 = nn.Linear(in_channels, in_channels)
        self.dropout2 = nn.Dropout(0.1)

    def forward(self, x: Tensor, pos_encoding: Optional[Tensor] = None):
        residual = self.norm1(x)

        kq = residual + pos_encoding.detach() if torch.is_tensor(pos_encoding) else residual
        residual = self.norm2(self.dropout1(self.mha(query=kq, key=kq, value=x)[0]))
        residual = self.fc2(F.relu(self.fc1(residual)))

        return x + self.dropout2(residual)


class DuplexAttentionLayer(nn.Module):
    def __init__(self, in_channels, n_inter_queries):
        super().__init__()

        self.norm1 = nn.LayerNorm(in_channels)
        self.mha_1 = nn.MultiheadAttention(in_channels, 8)
        self.dropout1 = nn.Dropout(0.1)

        self.norm2 = nn.LayerNorm(in_channels)
        self.fc1 = nn.Linear(in_channels, in_channels)

        self.mha_2 = nn.MultiheadAttention(in_channels, 8)
        self.dropout2 = nn.Dropout(0.1)
        self.norm3 = nn.LayerNorm(in_channels)

        self.fc2 = nn.Linear(in_channels, in_channels)
        self.fc3 = nn.Linear(in_channels, in_channels)

        self.dropout3 = nn.Dropout(0.1)

        # self.register_parameter("inter_q", nn.Parameter(
        #     torch.normal(0.0, 1.0, (n_inter_queries, 1, in_channels)), requires_grad=True))

    def forward(self, x: Tensor, inter_q: Tensor, pos_encoding: Optional[Tensor] = None):
        bs = x.shape[1]
        residual = self.norm1(x)

        k = residual + pos_encoding.detach() if torch.is_tensor(pos_encoding) else residual

        intermediate = self.norm2(self.dropout1(self.mha_1(query=inter_q.expand(-1, bs, -1), key=k, value=x)[0]))
        intermediate = F.relu(self.fc1(intermediate))

        q = k
        residual = self.norm3(self.dropout2(self.mha_2(query=q, key=intermediate, value=intermediate)[0]))

        residual = self.fc3(F.relu(self.fc2(residual)))

        return x + self.dropout3(residual), intermediate


@EMBEDDING_HEAD_REGISTRY.add("transformer")
class TransformerDecoder(nn.Module):
    def __init__(self, in_channels, inter_channels, embedding_size, tanh_activation,
                 seediness_output, experimental_dims, ConvType=nn.Conv3d,
                 PoolType=nn.AvgPool3d, NormType=nn.Identity):
        super().__init__()

        # PoolingLayerCallbacks = get_pooling_layer_creator(PoolType)
        # t_scales = get_temporal_scales()

        self.register_parameter("inter_q", nn.Parameter(
            torch.normal(0.0, 1.0, (64, 1, in_channels)), requires_grad=True))

        self.attn_32x = nn.ModuleList([
            DuplexAttentionLayer(inter_channels[0], 64),
            DuplexAttentionLayer(inter_channels[0], 64)
        ])

        self.conv_merge_16_32 = nn.Conv3d(inter_channels[0] + inter_channels[1], inter_channels[1], 1)

        # 16x
        self.conv_16x = nn.Identity()
        if inter_channels[1] != in_channels:
            self.conv_16x = nn.Conv3d(in_channels, inter_channels[1], 1, bias=False)

        self.attn_16x = nn.ModuleList([
            DuplexAttentionLayer(inter_channels[1], 64),
            DuplexAttentionLayer(inter_channels[1], 64)
        ])

        self.conv_merge_8_16 = nn.Conv3d(inter_channels[1] + inter_channels[2], inter_channels[2], 1)

        # 8x
        self.conv_8x = nn.Identity()
        if inter_channels[2] != in_channels:
            self.conv_8x = nn.Conv3d(in_channels, inter_channels[2], 1, bias=False)

        self.attn_8x = nn.ModuleList([
            DuplexAttentionLayer(inter_channels[2], 64),
            DuplexAttentionLayer(inter_channels[2], 64)
        ])

        self.conv_merge_4_8 = nn.Conv3d(inter_channels[2] + inter_channels[3], inter_channels[3], 1)

        # 4x
        self.conv_4x = nn.Identity()
        if inter_channels[3] != in_channels:
            self.conv_4x = nn.Conv3d(in_channels, inter_channels[3], 1, bias=False)

        # self.attn_4x = nn.ModuleList([
        #     DuplexAttentionLayer(inter_channels[3], 32),
        # ])
        self.block_4x = nn.Sequential(
            ConvType(inter_channels[3], inter_channels[3], 3, stride=1, padding=1),
            NormType(inter_channels[3]),
            nn.ReLU(inplace=True)
        )

        self.pos_encoders = nn.ModuleDict()
        for dim in set(inter_channels):
            self.pos_encoders[str(dim)] = SpatioTemporalEmbeddingSine(dim)

        self.embedding_size = embedding_size

        n_free_dims = get_nb_free_dims(experimental_dims)
        self.variance_channels = self.embedding_size - n_free_dims

        self.embedding_dim_mode = experimental_dims
        embedding_output_size = get_nb_embedding_dims(self.embedding_dim_mode)

        self.conv_embedding = nn.Conv3d(inter_channels[-1], embedding_output_size, kernel_size=1, padding=0, bias=False)
        self.conv_variance = nn.Conv3d(inter_channels[-1], self.variance_channels, kernel_size=1, padding=0, bias=True)

        self.conv_seediness, self.seediness_channels = None, 0
        if seediness_output:
            self.conv_seediness = nn.Conv3d(inter_channels[-1], 1, kernel_size=1, padding=0, bias=False)
            self.seediness_channels = 1

        self.tanh_activation = tanh_activation
        self.register_buffer("time_scale", torch.tensor(1.0, dtype=torch.float32))

    @staticmethod
    def flatten(x):
        """
        Flattens a spatio-temporal tensor to flat format
        :param x: tensor of shape [B, C, T, H, W]
        :return: tensor of shape [T*H*W, B, C]
        """
        return x.flatten(2).permute(2, 0, 1)

    @staticmethod
    def unflatten(x, t, h, w):
        x = x.permute(1, 2, 0)
        return x.view(*x.shape[:2], t, h, w)

    def forward(self, x):
        """
        :param x: list of multiscale feature map tensors of shape [N, C, T, H, W]. For this implementation, there
        should be 4 features maps in increasing order of spatial dimensions
        :return: embedding map of shape [N, E, T, H, W]
        """
        assert len(x) == 4, "Expected 4 feature maps, got {}".format(len(x))

        feat_map_32x, feat_map_16x, feat_map_8x, feat_map_4x = x
        inter_q = self.inter_q

        # 32x
        t, h, w = feat_map_32x.shape[-3:]
        pos_encoding = self.flatten(self.pos_encoders[str(feat_map_32x.shape[1])](feat_map_32x))
        x = self.flatten(feat_map_32x)
        for layer in self.attn_32x:
            x, inter_q = layer(x, inter_q, pos_encoding)
        x = self.unflatten(x, t, h, w)

        # 16x
        t, h, w = feat_map_16x.shape[-3:]
        feat_map_16x = self.conv_16x(feat_map_16x)
        x = F.interpolate(x, scale_factor=(1, 2, 2), mode='trilinear', align_corners=False)
        x = self.conv_merge_16_32(torch.cat((x, feat_map_16x), 1))

        pos_encoding = self.flatten(self.pos_encoders[str(x.shape[1])](x))
        x = self.flatten(x)
        for layer in self.attn_16x:
            x, inter_q = layer(x, inter_q, pos_encoding)
        x = self.unflatten(x, t, h, w)

        # 8x
        t, h, w = feat_map_8x.shape[-3:]
        feat_map_8x = self.conv_8x(feat_map_8x)
        x = F.interpolate(x, scale_factor=(1, 2, 2), mode='trilinear', align_corners=False)
        x = self.conv_merge_8_16(torch.cat((x, feat_map_8x), 1))

        pos_encoding = self.flatten(self.pos_encoders[str(x.shape[1])](x))
        x = self.flatten(x)
        for layer in self.attn_8x:
            x, inter_q = layer(x, inter_q, pos_encoding)
        x = self.unflatten(x, t, h, w)

        # 4x
        t, h, w = feat_map_4x.shape[-3:]
        feat_map_4x = self.conv_4x(feat_map_4x)
        x = F.interpolate(x, scale_factor=(1, 2, 2), mode='trilinear', align_corners=False)
        x = self.conv_merge_4_8(torch.cat((x, feat_map_4x), 1))

        # pos_encoding = self.flatten(self.pos_encoders[str(x.shape[1])](x))
        x = self.block_4x(x)
        # x = self.flatten(x)
        # for layer in self.attn_4x:
        #     x = layer(x, pos_encoding)
        # x = self.unflatten(x, t, h, w)

        embeddings = self.conv_embedding(x)
        if self.tanh_activation:
            embeddings = (embeddings * 0.25).tanh()

        embeddings = add_spatiotemporal_offset(embeddings, self.time_scale, self.embedding_dim_mode)

        variances = self.conv_variance(x)

        if self.conv_seediness is not None:
            seediness = self.conv_seediness(x).sigmoid()
            output = torch.cat((embeddings, variances, seediness), dim=1)
        else:
            output = torch.cat((embeddings, variances), dim=1)

        return output


def _test():
    T = 8
    H = 512
    W = 512
    C = 256

    x = [
        torch.zeros(1, C, T, H // 4, W // 4, dtype=torch.float32).cuda(),
        torch.zeros(1, C, T, H // 8, W // 8, dtype=torch.float32).cuda(),
        torch.zeros(1, C, T, H // 16, W // 16, dtype=torch.float32).cuda(),
        torch.zeros(1, C, T, H // 32, W // 32, dtype=torch.float32).cuda()
    ][::-1]

    decoder = TransformerDecoder(256, (256, 256, 128, 128), 4, True, True, "xyff").cuda()

    y = decoder(x)
    print(y.shape)


if __name__ == '__main__':
    _test()
